## Galactic Raiders -- A Lua/LOVE Learning Project

This README is horribly incomplete at the moment, but will (hopefully) be updated in the near future.

The purpose of this project is purely for my own education, to create a small, but full featured game in Lua and LOVE, in order to learn the language and API.

In the future, I hope to fully document the project, just in case anyone else might want to use the code in order to learn Lua and/or LOVE for themselves.

Stay Tuned!
