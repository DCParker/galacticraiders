

require "src.game"

function love.load()
  if arg[#arg] == "vsc_debug" then require("lldebugger").start() end
  -- Use this for ZeroBrane Studio
  --if arg[#arg] == "-debug" then
  --  require("mobdebug").start()
  --end
  love.math.setRandomSeed(love.timer.getTime())
  --love.window.setFullscreen(true, "exclusive")
  --love.mouse.setVisible(false)
  --love.mouse.setGrabbed(true)
  game = Game.new()
end

function love.update(dt)
  local status = dt <= 0.1 and game:update(dt)
end

function love.draw()
  game:render()
end

--[[
function love.keypressed(key)
  game:keyPressed(key)
end

function love.keyreleased(key)
  game:keyReleased(key)
end
]]--
