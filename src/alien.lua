

Alien = {
  index,
  curpos,
  curframe,
  parent,
  state,
  visible,
  sprites,
  shadows,
  XOFFSET = 16,
  YOFFSET = 16,
  APPROACHSPEED = 500,
  SHADOWCOUNT = 10,
  
  ACTIONSTATE = {
    ["UNSET"] = "UNSET",                    -- Blank state
    ["INITIALGRID"] = "INITIALGRID",        -- Initializing for the grid
    ["WAITGRID"] = "WAITGRID",              -- Waiting to approach grid
    ["APPROACHGRID"] = "APPROACHGRID",      -- Approach the grid from offscreen
    ["FOLLOWGRID"] = "FOLLOWGRID",          -- Move to the specified grid position
    ["DIVETOLOC"] = "DIVETOLOC",            -- Dive to a location and attack
    ["RETURNGRID"] = "RETURNGRID",          -- Return to specified grid position
    ["BOMBRUN"] = "BOMBRUN",                -- Dive to a location, drop bombs while moving horizontally to second location
    ["CROSSRUN"] = "CROSSRUN",              -- NON-GRID: Fly across screen, dropping bombs
    ["MULTIPOINTRUN"] = "MULTIPOINTRUN",    -- NON-GRID: Fly onto screen, to several points, then back off screen, dropping bombs
    ["MULTICROSSRUN"] = "MULTICROSSRUN"     -- NON-GRID: Fly across screen multiple times, dropping bombs
  }
}
Alien.__index = Alien


function Alien:new(index, parent, spritetype)
  local a = {}
  setmetatable(a, Alien)
  a.index = index                     -- Index position in AlienHandler
  a.curpos = {0, 0}                   -- Current screen position
  a.curframe = 1                      -- Current animation frame
  a.parent = parent                   -- Link to AlienHandler
  a.state = a.ACTIONSTATE.UNSET       -- Current behavior state
  a.visible = true                    -- Whether the alien is to be rendered
  a.sprites = {}                      -- Table to hold the sprite quads
  a.shadows = {}                      -- After images used for special effects
  
  return a
end

function Alien:clearFrames()
  self.sprites = {}
end

function Alien:addFrame(spritequad)
  table.insert(self.sprites, spritequad)
end

function Alien:setState(state)
  if self.ACTIONSTATE[state] ~= nil then
    self.state = self.ACTIONSTATE[state]
  else
    self.state = self.ACTIONSTATE["UNSET"]
  end
end

function Alien:update(dt)
  if self.state == self.ACTIONSTATE.INITIALGRID then
    self:handleInitialGrid(dt)
  elseif self.state == self.ACTIONSTATE.APPROACHGRID then
    self:handleApproachGrid(dt)
  elseif self.state == self.ACTIONSTATE.FOLLOWGRID then
    self:handleFollowGrid(dt)
  end
end

function Alien:handleInitialGrid(dt)
  local direction = math.random(3)
  self.curpos = self.parent:getCoordinates(self.index)
  if direction == 1 then
    self.curpos = {self.curpos[1], self.curpos[2] - 600}
    for i = 1,self.SHADOWCOUNT do
      table.insert(self.shadows,{self.curpos[1], self.curpos[2] - (i * 12), ((255 / self.SHADOWCOUNT) * (self.SHADOWCOUNT - i)) / 255})
    end
  elseif direction == 2 then
    self.curpos = {self.curpos[1] - 600, self.curpos[2]}
    for i = 1,self.SHADOWCOUNT do
      table.insert(self.shadows,{self.curpos[1] - (i * 12), self.curpos[2], ((255 / self.SHADOWCOUNT) * (self.SHADOWCOUNT - i)) / 255})
    end
  elseif direction == 3 then
    self.curpos = {self.curpos[1] + 600, self.curpos[2]}
    for i = 1,self.SHADOWCOUNT do
      table.insert(self.shadows,{self.curpos[1] + (i * 12), self.curpos[2], ((255 / self.SHADOWCOUNT) * (self.SHADOWCOUNT - i)) / 255})
    end
  end
  self.state = self.ACTIONSTATE["WAITGRID"]
end

function Alien:handleApproachGrid(dt)
  local gotopos = self.parent:getCoordinates(self.index)
  self.curframe = (math.floor(self.parent:getAnimationStep() / 5) % 2) + 1
  
  if self.curpos[1] > gotopos[1] + (self.APPROACHSPEED * dt) then
    self.curpos[1] = self.curpos[1] - (self.APPROACHSPEED * dt)
  elseif self.curpos[1] < gotopos[1] - (self.APPROACHSPEED * dt) then
    self.curpos[1] = self.curpos[1] + (self.APPROACHSPEED * dt)
  else
    self.curpos[1] = gotopos[1]
  end
  
  if self.curpos[2] > gotopos[2] + (self.APPROACHSPEED * dt) then
    self.curpos[2] = self.curpos[2] - (self.APPROACHSPEED * dt)
  elseif self.curpos[2] < gotopos[2] - (self.APPROACHSPEED * dt) then
    self.curpos[2] = self.curpos[2] + (self.APPROACHSPEED * dt)
  else
    self.curpos[2] = gotopos[2]
  end
  
  for i = #self.shadows,1,-1 do
    local shdpos = self.shadows[i]
    local xmatch = false
    local ymatch = false
    if shdpos[1] > gotopos[1] + (self.APPROACHSPEED * dt) then
      shdpos[1] = shdpos[1] - (self.APPROACHSPEED * dt)
    elseif shdpos[1] < gotopos[1] - (self.APPROACHSPEED * dt) then
      shdpos[1] = shdpos[1] + (self.APPROACHSPEED * dt)
    else
      xmatch = true
    end
    
    if shdpos[2] > gotopos[2] + (self.APPROACHSPEED * dt) then
      shdpos[2] = shdpos[2] - (self.APPROACHSPEED * dt)
    elseif shdpos[2] < gotopos[2] - (self.APPROACHSPEED * dt) then
      shdpos[2] = shdpos[2] + (self.APPROACHSPEED * dt)
    else
      ymatch = true
    end
    
    if xmatch and ymatch then
      table.remove(self.shadows, i)
    else
      self.parent:addAlienSprite(
          self.sprites[self.curframe],
          shdpos[1],
          shdpos[2],
          self.XOFFSET,
          self.YOFFSET,
          {255,0,0,shdpos[3]})
      self.shadows[i] = shdpos
    end
  end
  
  love.graphics.setColor(255, 255, 255, 255)
  self.parent:addAlienSprite(
      self.sprites[self.curframe],
      self.curpos[1],
      self.curpos[2],
      self.XOFFSET,
      self.YOFFSET,
      {255,255,255,255})
    
  if #self.shadows == 0 then
    self.state = self.ACTIONSTATE["FOLLOWGRID"]
    self.parent:reportState(self.index, self.state)
  end
end

function Alien:handleFollowGrid(dt)
  self.curframe = (math.floor(self.parent:getAnimationStep() / 5) % 2) + 1
  self.curpos = self.parent:getCoordinates(self.index)
  if self.visible then
    self.parent:addAlienSprite(
        self.sprites[self.curframe],
        self.curpos[1],
        self.curpos[2],
        self.XOFFSET,
        self.YOFFSET)
  end
end
