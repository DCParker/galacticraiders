
require "src.alien"
local Utilities = require "src.utilities"

AlienHandler = {
  image,
  grid,
  gridx,
  gridy,
  renderwidth,
  renderheight,
  sprites,
  animstep,
  state,
  timer,
  approachorder,
  movepattern,
  movedirection,
  travelval,
  SPRITE_WIDTH = 32,
  SPRITE_HEIGHT = 32,
  GRID_WIDTH = 10,
  GRID_HEIGHT = 6,
  GRID_SPEED = 10,
  MAX_TIMER = 2,
  TIMER_MINUS = 0.1,
  APPROACH_COUNTDOWN = 2,
  MIN_BOUND = 20,
  MAX_BOUND = 620,
  BOTTOM_BOUND = 416,
  HORIZ_MIDPOINT = 320,
  
  ALIEN_SPRITE_COORDS = {
    {{128,0},{160,0}},
    {{320,0},{0,32}},
    {{64,0},{96,0}},
    {{256,0},{288,0}},
    {{192,0},{224,0}},
    {{0,0},{32,0}}
  },
  
  MOTHERSHIP_SPRITE_COORDS = {
    {{128,64},{256,64}},
    {{160,64},{288,64}},
    {{192,64},{320,64}},
    {{224,64},{0,96}}
  },
  
  GRID_MOVE_PATTERNS = {
    ["SIDESIDEDOWN"] = "SIDESIDEDOWN",
    ["STRETCHSIDEDOWN"] = "STRETCHSIDEDOWN",
    ["EXPANDCONTRACTDOWN"] = "EXPANDCONTRACTDOWN"
  },
  
  ALIEN_HANDLER_STATES = {
    ["WAIT"] = "WAIT",
    ["INITIALIZEGRID"] = "INITIALIZEGRID",
    ["FILLGRID"] = "FILLGRID",
    ["MOVEGRID"] = "MOVEGRID",
    ["GRIDCLEAR"] = "GRIDCLEAR",
    ["GRIDBOTTOM"] = "GRIDBOTTOM"
  }
}
AlienHandler.__index = AlienHandler


function AlienHandler:new()
  local ah = {}
  setmetatable(ah, AlienHandler)
  ah.image = nil
  ah.grid = {}
  ah.gridx = 320
  ah.gridy = 160
  ah.sprites = nil
  ah.animstep = 0
  ah.state = ah.ALIEN_HANDLER_STATES.WAIT
  ah.timer = ah.MAX_TIMER
  ah.movepattern = ah.GRID_MOVE_PATTERNS.SIDESIDEDOWN
  ah.movedirection = 0
  ah.travelval = 0
  ah.renderwidth = ah.SPRITE_WIDTH
  ah.renderheight = ah.SPRITE_HEIGHT
  
  return ah
end

function AlienHandler:setTextureImage(img)
  self.image = img
  self.sprites = love.graphics.newSpriteBatch(img)
end

function AlienHandler:getState()
  return self.state
end

function AlienHandler:setState(state)
  if self.ALIEN_HANDLER_STATES[state] ~= nil then
    self.state = self.ALIEN_HANDLER_STATES[state]
  end
end

function AlienHandler:fillAlienGrid()
  local counter = 1
  for y = 1, self.GRID_HEIGHT do
    for x = 1, self.GRID_WIDTH do
      local alien = Alien:new(counter, self, y)
      alien:setState("INITIALGRID")
      local coordset = self.ALIEN_SPRITE_COORDS[y]
      for indexc, coords in ipairs(coordset) do
        alien:addFrame(love.graphics.newQuad(coords[1], coords[2], self.SPRITE_WIDTH, self.SPRITE_HEIGHT, self.image:getDimensions()))
      end
      table.insert(self.grid, alien)
      counter = counter + 1
    end
  end
end

function AlienHandler:getCoordinates(index)
  local xcoord = self.gridx - (self.GRID_WIDTH * self.renderwidth / 2) + (math.floor((index - 1) % self.GRID_WIDTH) * self.renderwidth) + math.floor(self.SPRITE_WIDTH / 2)
  local ycoord = self.gridy - (self.GRID_HEIGHT * self.renderheight / 2) + (math.floor((index - 1) / self.GRID_WIDTH) * self.renderheight) + math.floor(self.SPRITE_HEIGHT / 2)
  return {xcoord, ycoord}
end

function AlienHandler:update(dt)
  if self.state == self.ALIEN_HANDLER_STATES.FILLGRID then
    self:handleFillGrid(dt)
    self.state = self.ALIEN_HANDLER_STATES.APPROACHGRID
  elseif self.state == self.ALIEN_HANDLER_STATES.APPROACHGRID then
    self:handleApproachGrid(dt)
  elseif self.state == self.ALIEN_HANDLER_STATES.MOVEGRID then
    self:handleMoveGrid(dt)
  end
end

function AlienHandler:render()
  love.graphics.draw(self.sprites)
end

function AlienHandler:addAlienSprite(spritequad, x, y, xoffset, yoffset, color)
  if color ~= nil then
    self.sprites:setColor(color[1],color[2],color[3],color[4])
  end
  self.sprites:add(spritequad, x, y, 0, 1, 1, xoffset, yoffset, 0, 0)
end

function AlienHandler:getAnimationStep()
  return math.floor(self.animstep)
end

function AlienHandler:reportState(index, newstate)

end

function AlienHandler:handleFillGrid(dt)
  self.grid = {}
  self.approachorder = {}
  self:fillAlienGrid()
  for i = 1,#self.grid do
    table.insert(self.approachorder, i)
  end
  
  self.approachorder = Utilities.shuffle(self.approachorder)
end

function AlienHandler:handleApproachGrid(dt)
  if self.timer <= 0 and #self.approachorder > 0 then
    self.timer = self.MAX_TIMER - (self.TIMER_MINUS * (#self.grid - #self.approachorder))
    self.grid[self.approachorder[1]]:setState("APPROACHGRID")
    table.remove(self.approachorder, 1)
  elseif #self.approachorder == 0 then
    self.state = self.ALIEN_HANDLER_STATES.MOVEGRID
    -- Get grid move pattern
    -- Set to side-side-down by default here
    --self.movepattern = self.GRID_MOVE_PATTERNS.EXPANDCONTRACTDOWN
    self.movepattern = self.GRID_MOVE_PATTERNS.STRETCHSIDEDOWN

    self.movedirection = 1
  else
    self.timer = self.timer - (self.APPROACH_COUNTDOWN * dt)
  end
  
  self.sprites:clear()
  if #self.grid > 0 then
    for index, alien in ipairs(self.grid) do
      alien:update(dt)
    end
  end
  self.sprites:flush()
  self.animstep = self.animstep + (5 * dt)
  while self.animstep >= 10 do
    self.animstep = self.animstep - 10
  end
end

function AlienHandler:handleMoveGrid(dt)
  self.sprites:clear()
  
  if self.movepattern == self.GRID_MOVE_PATTERNS.SIDESIDEDOWN then
    self:handleSideSideDownMovement(dt)
  elseif self.movepattern == self.GRID_MOVE_PATTERNS.STRETCHSIDEDOWN then
    self:handleStretchSideDownMovement(dt)
  elseif self.movepattern == self.GRID_MOVE_PATTERNS.EXPANDCONTRACTDOWN then
    self:handleExpandContractDownMovement(dt)
  end
  
  if #self.grid > 0 then
    for index, alien in ipairs(self.grid) do
      alien:update(dt)
    end
  end
  self.sprites:flush()
  self.animstep = self.animstep + (5 * dt)
  while self.animstep >= 10 do
    self.animstep = self.animstep - 10
  end
end

function AlienHandler:handleSideSideDownMovement(dt)
  if self.movedirection == 1 then
    self.gridx = self.gridx + (self.GRID_SPEED * dt)
    if self.gridx + (self.GRID_WIDTH * self.SPRITE_WIDTH / 2) >= self.MAX_BOUND then
      self.gridx = self.MAX_BOUND - (self.GRID_WIDTH * self.SPRITE_WIDTH / 2)
      self.movedirection = 2
    end
  elseif self.movedirection == 2 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT then
      self.gridy = self.gridy - (self.travelval - self.SPRITE_HEIGHT)
      self.travelval = 0
      self.movedirection = 3
    end
  elseif self.movedirection == 3 then
    self.gridx = self.gridx - (self.GRID_SPEED * dt)
    if self.gridx - (self.GRID_WIDTH * self.SPRITE_WIDTH / 2) <= self.MIN_BOUND then
      self.gridx = self.MIN_BOUND + (self.GRID_WIDTH * self.SPRITE_WIDTH / 2)
      self.movedirection = 4
    end
  elseif self.movedirection == 4 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT then
      self.gridy = self.gridy - (self.travelval - self.SPRITE_HEIGHT)
      self.travelval = 0
      self.movedirection = 1
    end
  end
  if self.gridy >= self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2) then
    self.gridy = self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2)
    self.movedirection = 0
  end
end

function AlienHandler:handleStretchSideDownMovement(dt)
  if self.movedirection == 1 then
    self.renderwidth = self.renderwidth + (self.GRID_SPEED * dt / 2)
    if self.renderwidth * self.GRID_WIDTH >= self.MAX_BOUND - self.MIN_BOUND then
      self.renderwidth = (self.MAX_BOUND - self.MIN_BOUND) / self.GRID_WIDTH
      self.movedirection = 2
    end
  elseif self.movedirection == 2 then
    if self.gridx < self.HORIZ_MIDPOINT then
      self.renderwidth = self.renderwidth + (self.GRID_SPEED * dt / 2)
      self.gridx = self.MIN_BOUND + (self.renderwidth * self.GRID_WIDTH / 2)
    elseif self.gridx < self.MAX_BOUND - (self.SPRITE_WIDTH * self.GRID_WIDTH / 2) then
      self.renderwidth = self.renderwidth - (self.GRID_SPEED * dt / 2)
      self.gridx = self.MAX_BOUND - (self.renderwidth * self.GRID_WIDTH / 2)
    else
      self.renderwidth = self.SPRITE_WIDTH
      self.gridx = self.MAX_BOUND - (self.renderwidth * self.GRID_WIDTH / 2)
      self.movedirection = 3
    end
  elseif self.movedirection == 3 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT then
      self.gridy = self.gridy - (self.travelval - self.SPRITE_HEIGHT)
      self.travelval = 0
      self.movedirection = 4
    end
  elseif self.movedirection == 4 then
    if self.gridx > self.HORIZ_MIDPOINT then
      self.renderwidth = self.renderwidth + (self.GRID_SPEED * dt / 2)
      self.gridx = self.MAX_BOUND - (self.renderwidth * self.GRID_WIDTH / 2)
    elseif self.gridx > self.MIN_BOUND + (self.SPRITE_WIDTH * self.GRID_WIDTH / 2) then
      self.renderwidth = self.renderwidth - (self.GRID_SPEED * dt / 2)
      self.gridx = self.MIN_BOUND + (self.renderwidth * self.GRID_WIDTH / 2)
    else
      self.renderwidth = self.SPRITE_WIDTH
      self.gridx = self.MIN_BOUND + (self.renderwidth * self.GRID_WIDTH / 2)
      self.movedirection = 5
    end
  elseif self.movedirection == 5 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT then
      self.gridy = self.gridy - (self.travelval - self.SPRITE_HEIGHT)
      self.travelval = 0
      self.movedirection = 2
    end
  end
  if self.gridy >= self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2) then
    self.gridy = self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2)
    self.movedirection = 0
  end
end

function AlienHandler:handleExpandContractDownMovement(dt)
  if self.movedirection == 1 then
    self.renderwidth = self.renderwidth + (self.GRID_SPEED * dt / 2)
    if self.renderwidth * self.GRID_WIDTH >= self.MAX_BOUND - self.MIN_BOUND then
      self.renderwidth = (self.MAX_BOUND - self.MIN_BOUND) / self.GRID_WIDTH
      self.movedirection = 2
    end
  elseif self.movedirection == 2 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT / 2 then
      self.gridy = self.gridy - (self.travelval - (self.SPRITE_HEIGHT / 2))
      self.travelval = 0
      self.movedirection = 3
    end
  elseif self.movedirection == 3 then
    self.renderwidth = self.renderwidth - (self.GRID_SPEED * dt / 2)
    if self.renderwidth < self.SPRITE_WIDTH then
      self.renderwidth = self.SPRITE_WIDTH
      self.movedirection = 4
    end
  elseif self.movedirection == 4 then
    self.gridy = self.gridy + (self.GRID_SPEED * dt)
    self.travelval = self.travelval + (self.GRID_SPEED * dt)
    if self.travelval >= self.SPRITE_HEIGHT / 2 then
      self.gridy = self.gridy - (self.travelval - (self.SPRITE_HEIGHT / 2))
      self.travelval = 0
      self.movedirection = 1
    end
  end
  if self.gridy >= self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2) then
    self.gridy = self.BOTTOM_BOUND - (self.GRID_HEIGHT * self.SPRITE_HEIGHT / 2)
    self.movedirection = 0
  end
end
