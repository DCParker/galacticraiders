

Background = {
  photobg,
  blankbg,
  bgtype
}
Background.__index = Background


function Background:new()
  local bg = {}
  setmetatable(bg, Background)
  bg.bgtype = 1
  
  return bg
end

function Background:render()
  if self.bgtype == 1 and self.photobg ~= nil then
    love.graphics.draw(self.photobg, 0, 0)
  elseif self.bgtype == 2 and not self.blankbg ~= nil then
    love.graphics.draw(self.blankbg, 0, 0)
  else
    return
  end
end

function Background:setPhotoBackgroundImage(image)
  -- XXXXXXXXXXXXXXXXXXXXX
  -- Change this, so that it checks the image dimensions
  if true then
    self.photobg = image
  end
end

function Background:setBlankBackgroundImage(image)
  -- XXXXXXXXXXXXXXXXXXXXX
  -- Change this, so that it checks the image dimensions
  if true then
    self.blankbg = image
  end
end

function Background:setBackgroundType(bgtype)
  self.bgtype = bgtype
end
