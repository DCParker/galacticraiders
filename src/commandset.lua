
require "src.controlset"

--[[
  CommandSet
      Stores all controls for a single command, such
      as moving in a direction or firing.
]]--
CommandSet = {
    controls
}
CommandSet.__index = CommandSet


function CommandSet:new()
    local cs = {}
    setmetatable(cs, CommandSet)

    cs.controls = {}

    return cs
end

function CommandSet:addControlSet(cs)
    table.insert(self.controls, cs)
end

function CommandSet:clearControlSets()
    self.controls = {}
end

function CommandSet:isCommandActive()
    local status = false
    for i,control in ipairs(self.controls) do
        if control.source == "keyboard" then
            status = self:checkKeyboardCommandActive(control, status)
        elseif control.source == "joystick" then
            status = self:checkJoystickCommandActive(control, status)
        end
    end
    return status
end

function CommandSet:checkKeyboardCommandActive(control, status)
    local key = control:getComponent()
    if love.keyboard.isDown(key) then
        return true
    else
        return status
    end
end

function CommandSet:checkJoystickCommandActive(control, status)
    local joystick = love.joystick.getJoysticks()[control:getIndex()]
    if joystick ~= nil then
        if control:getComponent() == "axis" then
            local axval = joystick:getGamepadAxis(control:getIdentifier())
            if control:getComparator() == "lessthan" then
                if axval < control:getThreshold() then
                    return true
                end
            elseif control:getComparator() == "greaterthan" then
                if axval > control:getThreshold() then
                    return true
                end
            end
        elseif control:getComponent() == "button" then
            if joystick:isGamepadDown(control:getIdentifier()) then
                return true
            end
        end
    end
    return status
end