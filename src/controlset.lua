--[[
  ControlSet
      Stores a single control value for
      reference by the game during gameplay,
      such as a keystroke to represent left
      or a button press to represent fire.
]]--
ControlSet = {
  source,       -- The source of the control input, i.e. keyboard, joystick controller, etc.
  index,        -- The number of the control input, if there are more than one
  component,    -- The component of the source to be read, i.e. axis, button, d-pad
  identifier,   -- Indentifier for the component, if necessary
  threshold,    -- The threshold value for the input, if it is analog
  comparator    -- Whether a value should be checked to be above or below the threshold
}
ControlSet.__index = ControlSet


function ControlSet:new()
  local cs = {}
  setmetatable(cs, ControlSet)
  
  cs.source = nil
  cs.index = 0
  cs.component = nil
  cs.identifier = nil
  cs.threshold = 0.0
  cs.comparator = nil
  
  return cs
end

function ControlSet:getSource()
  return self.source
end

function ControlSet:getIndex()
  return self.index
end

function ControlSet:getComponent()
  return self.component
end

function ControlSet:getIdentifier()
  return self.identifier
end

function ControlSet:getThreshold()
  return self.threshold
end

function ControlSet:getComparator()
  return self.comparator
end

function ControlSet:setValues(source, index, component, identifier, threshold, comparator)
  self.source = source
  self.index = index
  self.component = component
  self.identifier = identifier
  self.threshold = threshold
  self.comparator = comparator
end

function ControlSet:setKeyboardValue(component)
  self.source = "keyboard"
  self.index = 0
  self.component = component
  self.identifier = nil
  self.threshold = 0.0
  self.comparator = nil
end

