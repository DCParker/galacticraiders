
require "src.background"
require "src.resourcehandler"
require "src.alienhandler"
require "src.player"
require "src.shottracker"
require "src.settings"

Game = {
  rh,
  bg,
  ah,
  pl,
  st,
  set
}
Game.__index = Game
  
function Game:new()
  local g = {}
  setmetatable(g, Game)
  g.bg = Background.new()
  g.rh = ResourceHandler.new()
  g.ah = AlienHandler.new()
  g.pl = Player.new()
  g.st = ShotTracker.new()
  g.pl:setShotTracker(g.st)
  g.rh:loadBackgroundResources(g.bg)
  g.rh:loadAlienHandlerResources(g.ah)
  g.rh:loadPlayerResources(g.pl)
  g.rh:loadShotTrackerResources(g.st)
  
  love.graphics.setDefaultFilter("nearest")
  
  g.ah:setState("FILLGRID")
  
  g.set = Settings.new()
  g.pl:setSettings(g.set)
  
  return g
end

function Game:update(dt)
  self.ah:update(dt)
  self.pl:update(dt)
  self.st:update(dt)
end

function Game:render()
  self.bg:render()
  self.ah:render()
  self.pl:render()
  self.st:render()
end
  
--[[
function Game:keyPressed(key)
  if key == self.set:getLeftKey() then
    self.playermovement = self.playermovement - 1
    if self.playermovement < -1 then
      self.playermovement = -1
    end
  end
  if key == self.set:getRightKey() then
    self.playermovement = self.playermovement + 1
    if self.playermovement > 1 then
      self.playermovement = 1
    end
  end
end

function Game:keyReleased(key)
  if key == "space" then
    if currentbg == 1 then
      currentbg = 2
    else
      currentbg = 1
    end
    self.bg:setBackgroundType(currentbg)
  end
  if key == "escape" then
    love.event.quit(0)
  end
  if key == self.set:getLeftKey() then
    self.playermovement = self.playermovement + 1
    if self.playermovement > 1 then
      self.playermovement = 1
    end
  end
  if key == self.set:getRightKey() then
    self.playermovement = self.playermovement - 1
    if self.playermovement < -1 then
      self.playermovement = -1
    end
  end
end
]]--

