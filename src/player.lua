
require "src.settings"

Player = {
  xpos = 320,
  xoffset = 16,
  yoffset = 32,
  cannonimg = nil,
  settings = nil,
  cooldown = 0,
  shottracker = nil,
  commands = nil,
  MAX_COOLDOWN = 0.5,
  YPOS = 464,
  CANNON_IMGPOS_X = 64,
  CANNON_IMGPOS_Y = 64,
  CANNON_IMGWIDTH = 32,
  CANNON_IMGHEIGHT = 32,
  MOVE_SPEED = 160,
  MIN_BOUND = 20,
  MAX_BOUND = 620,
  testval = nil
}
Player.__index = Player

function Player:new()
  local p = {}
  setmetatable(p, Player)
  local settings = Settings:new()
  p.commands = settings:getCommandSet()

  return p
end

function Player:setSettings(settings)
  self.settings = settings
end

function Player:setTextureImage(img)
  local tmpcanvas = love.graphics.newCanvas(32, 32)
  love.graphics.setCanvas(tmpcanvas)
  love.graphics.clear()
  local cannonquad = love.graphics.newQuad(
    self.CANNON_IMGPOS_X,
    self.CANNON_IMGPOS_Y,
    self.CANNON_IMGWIDTH,
    self.CANNON_IMGHEIGHT,
    img:getDimensions())
  love.graphics.draw(img, cannonquad, 0, 0)

  love.graphics.setCanvas()

  self.cannonimg = love.graphics.newImage(tmpcanvas:newImageData())
end

function Player:setShotTracker(st)
  self.shottracker = st
end

function Player:update(dt)
  local direction = 0

  local command = self.commands["move_left"]
  if command:isCommandActive() then
    direction = direction - 1
  end

  command = self.commands["move_right"]
  if command:isCommandActive() then
    direction = direction + 1
  end

  command = self.commands["fire"]
  if command:isCommandActive() then
    self.shottracker:addShot(self.xpos - self.xoffset, self.YPOS - self.yoffset)
  end

  self.xpos = self.xpos + (dt * self.MOVE_SPEED * direction)
  if self.xpos < self.MIN_BOUND + self.xoffset then
    self.xpos = self.MIN_BOUND + self.xoffset
  end
  if self.xpos > self.MAX_BOUND - self.CANNON_IMGWIDTH + self.xoffset then
    self.xpos = self.MAX_BOUND - self.CANNON_IMGWIDTH + self.xoffset
  end
end

function Player:render()
  love.graphics.draw(self.cannonimg, self.xpos - self.xoffset, self.YPOS - self.yoffset)
  if testval ~= nil then
    love.graphics.setColor(1,1,1,1)
    love.graphics.print(testval, 10, 10)
  end
end
