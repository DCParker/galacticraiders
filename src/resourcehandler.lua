
ResourceHandler = {}
ResourceHandler.__index = ResourceHandler

function ResourceHandler:new()
  local rh = {}
  setmetatable(rh, ResourceHandler)
  
  return rh
end

function ResourceHandler:loadBackgroundResources(bg)
  bg:setPhotoBackgroundImage(love.graphics.newImage("assets/art/aliensky.png"))
  bg:setBlankBackgroundImage(love.graphics.newImage("assets/art/blacksky.png"))
  bg:setBackgroundType(1)
end

function ResourceHandler:loadAlienHandlerResources(ah)
  ah:setTextureImage(love.graphics.newImage("assets/art/invaders.png"))
end

function ResourceHandler:loadPlayerResources(pl)
  pl:setTextureImage(love.graphics.newImage("assets/art/invaders.png"))
end

function ResourceHandler:loadShotTrackerResources(st)
  st:setTextureImage(love.graphics.newImage("assets/art/invaders.png"))
end
