
require "src.commandset"

Settings = {
  commandset
}
Settings.__index = Settings

function Settings:new()
  local s = {}
  setmetatable(s, Settings)
  
  s.commandset = {}

  local move_left = CommandSet:new()
  local left_move_1 = ControlSet:new()
  left_move_1:setValues("keyboard", 0, "left", nil, 0.0, nil)
  move_left:addControlSet(left_move_1)
  local left_move_2 = ControlSet:new()
  left_move_2:setValues("joystick", 1, "axis", "leftx", -0.3, "lessthan")
  move_left:addControlSet(left_move_2)
  table.insert(s.commandset, "move_left")
  s.commandset["move_left"] = move_left
  
  local move_right = CommandSet:new()
  local right_move_1 = ControlSet:new()
  right_move_1:setValues("keyboard", 0, "right", nil, 0.0, nil)
  move_right:addControlSet(right_move_1)
  local right_move_2 = ControlSet:new()
  right_move_2:setValues("joystick", 1, "axis", "leftx", 0.3, "greaterthan")
  move_right:addControlSet(right_move_2)
  table.insert(s.commandset, "move_right")
  s.commandset["move_right"] = move_right
  
  local fire = CommandSet:new()
  local fire1 = ControlSet:new()
  fire1:setValues("keyboard", 0, "space", nil, 0.0, nil)
  fire:addControlSet(fire1)
  local fire2 = ControlSet:new()
  fire2:setValues("joystick", 1, "button", "a", 0.0, nil)
  fire:addControlSet(fire2)
  table.insert(s.commandset, "fire")
  s.commandset["fire"] = fire

  local quit = CommandSet:new()
  local quit1 = ControlSet:new()
  quit1:setValues("keyboard", 0, "escape", nil, 0.0, nil)
  quit:addControlSet(quit1)
  table.insert(s.commandset, "quit")
  s.commandset["quit"] = quit
  
  return s
end

function Settings:getCommandSet()
  return self.commandset
end
