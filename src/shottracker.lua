

ShotTracker = {
    shots = {},
    maxshots = 3,
    cooldown = 0.0,
    shotquad = nil,
    MAX_COOLDOWN = 0.5,
    SPEED = 200,
    MIN_Y = -10,
    X_OFFSET = 16,
    Y_OFFSET = 16,
    SHOT_IMGPOS_X = 96,
    SHOT_IMGPOS_Y = 64,
    SHOT_IMGWIDTH = 32,
    SHOT_IMGHEIGHT = 32,
}
ShotTracker.__index = ShotTracker


function ShotTracker:new()
    local st = {}
    setmetatable(st, ShotTracker)

    return st
end

function ShotTracker:addShot(x, y)
    if #self.shots < self.maxshots and self.cooldown <= 0.0 then
        local shot = {}
        shot["x"] = x
        shot["y"] = y
        table.insert(self.shots, shot)
        self.cooldown = self.MAX_COOLDOWN
    end
end

function ShotTracker:update(dt)
    if #self.shots > 0 then
        for i,shot in ipairs(self.shots) do
            shot["y"] = shot["y"] - (dt * self.SPEED)
            if shot["y"] < self.MIN_Y then
                table.remove(self.shots, i)
            end
        end
    end
    if self.cooldown > 0.0 then
        self.cooldown = self.cooldown - dt
    end
end

function ShotTracker:render()
    for i,shot in ipairs(self.shots) do
        love.graphics.draw(self.shotimg, shot["x"], shot["y"])
    end
  end

function ShotTracker:setTextureImage(img)
    local tmpcanvas = love.graphics.newCanvas(32, 32)
    love.graphics.setCanvas(tmpcanvas)
    love.graphics.clear()
    self.shotquad = love.graphics.newQuad(
      self.SHOT_IMGPOS_X,
      self.SHOT_IMGPOS_Y,
      self.SHOT_IMGWIDTH,
      self.SHOT_IMGHEIGHT,
      img:getDimensions())
    love.graphics.draw(img, self.shotquad, 0, 0)
  
    love.graphics.setCanvas()
  
    self.shotimg = love.graphics.newImage(tmpcanvas:newImageData())
end
