

local Utilities = {}

function Utilities.shuffle(table)
  for pass = 1,7 do
    for index = 1,#table do
      rindex = math.random(1,#table)
      oldval = table[rindex]
      table[rindex] = table[index]
      table[index] = oldval
    end
  end
  return table
end

return Utilities
